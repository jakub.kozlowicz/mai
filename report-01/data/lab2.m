%% Distribution function
x = -3:0.01:3;
f = (1 / sqrt(2 * pi)) * exp(-0.5 * x.^2);
g = 0.5 * exp(-abs(x));
cg = sqrt(2*exp(1)/pi) * g;
figure;
hold on;
grid on;
plot(x, f, 'b', 'LineWidth', 2, 'DisplayName', 'f(x)');
plot(x, g, 'r', 'LineWidth', 2, 'DisplayName', 'g(x)');
plot(x, cg, 'g', 'LineWidth', 2, 'DisplayName', 'cg(x)');
xlabel('x');
ylabel('f(x) and g(x)');
title('Gaussian and Exponential Distribution Functions');
xlim([-3, 3]);
ylim([-0.1, 1]);
legend;

%% Random number generation
x = 1:1000;
no_x = 1;

while no_x <= 10000
    random = -log(rand());
    if rand() < 0.5
        random = -random;
    end
    
    if ((abs(random) - 1)^2) <= (-2*log(rand()))
        x(no_x) = random;
        no_x = no_x + 1;
    end
end
figure;
histogram(x, 50);