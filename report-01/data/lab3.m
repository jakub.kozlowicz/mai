close all;
clear;

N = linspace(100, 1000, 10);
R = 200;
theta_s = 2;
MSEn = zeros(length(N), 1);

for i = 1:length(N)
    n = N(i);
    theta_n = zeros(R, 1);
    for j = 1:R
        theta_k = zeros(n, 1);
        for k = 1:n
            theta_k(k) = theta_s + (rand() - 0.5);
        end
        theta_n(j) = sum(theta_k) / n;
    end
    
    for j = 1:R
        MSEn(i) = MSEn(i) + (theta_n(j) - theta_s)^2;
    end
    MSEn(i) = MSEn(i) / R;
end

MSEt = zeros(length(N), 1);
for i = 1:length(N)
    MSEt(i) = ((1/12)/N(i));
end
 
figure;
hold on;
grid on;
plot(N, MSEn, "Color", "red", "LineStyle", "none", "Marker", "x", "LineWidth", 2);
plot(N, MSEt, "Color", "blue", "LineStyle", "none", "Marker", "o", "LineWidth", 2);
title("Number of realizations R = " + R);
xlabel("Number of N");
ylabel("Value of MSE");
legend("Computed", "Theoretical");