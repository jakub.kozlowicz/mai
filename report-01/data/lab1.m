%% Distribution function
x = -2:0.01:2;
f = zeros(size(x));
f(x >= -1 & x <= 0) = -x(x >= -1 & x <= 0);
f(x > 0 & x <= 1) = x(x > 0 & x <= 1);

figure;
plot(x, f, 'LineWidth', 2);
xlabel('x');
ylabel('f(x)');
title('Distribution function - f(x)');

grid on;
xlim([-2, 2]);
ylim([-0.5, 1.5]);

%% Cumulative distribution function
x = -2:0.01:2;
F = zeros(size(x));
F(x >= -1 & x <= 0) = -(1/2) * x(x >= -1 & x <= 0).^2 + 1/2;
F(x > 0 & x <= 1) = (1/2) * x(x > 0 & x <= 1).^2 + 1/2;
F(x > 1) = 1;
figure;
plot(x, F, 'LineWidth', 2);
xlabel('x');
ylabel('F(x)');
title('Cumulative Distribution Function - F(x)');
grid on;
xlim([-2, 2]);
ylim([-0.5, 1.5]);


%% Random number generation
x = 1:1000;
for i = 1:1000
    random = rand();
    if random <= 0.5
        value = -sqrt(-2*random+1);
    else
        value = sqrt(2*random-1);
    end
    x(i) = value;
end

figure;
histogram(x, 30);