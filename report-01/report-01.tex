\documentclass[11pt, a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage[left = 2cm,right = 2cm,top = 2cm,bottom = 3cm]{geometry}

\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{float}
\usepackage{graphicx}
\usepackage{multicol}
\usepackage{multirow}
\usepackage{units}
\usepackage{mwe}
\usepackage{graphbox}
\usepackage{verbatim}
\usepackage{fancyvrb}
\usepackage{array}
\usepackage{pdfpages}
\usepackage{booktabs}
\usepackage{setspace}
\usepackage{flafter}
\usepackage{minted}
\usepackage{caption}
\usepackage{subcaption}

\usepackage{report_pwr}

\setminted[matlab]{
    frame=lines,
    framesep=2mm,
    baselinestretch=1.0,
    fontsize=\footnotesize,
    linenos
}

\title{Report 1}
\lecture{Modeling and Identification}
\term{Wednesday 13:15--15:00}
\author{\foreignlanguage{polish}{Jakub Kozłowicz}, 252865}
\lecturer{\foreignlanguage{polish}{Grzegorz Mzyk}, Ph.D., D.Sc., Assoc. Prof.}
\date{October 2023}

\begin{document}

\maketitle
% \tableofcontents
% \pagebreak

\section{Random number generators}

First two laboratory exercises were about random number generators. In the first
one, the assignment was to implement inversion method for generating random
numbers from a given distribution. In the second one, the assignment was to
implement acceptance-rejection method for generating random numbers from a given
distribution.

\subsection{Inversion method}

The inversion method is a method for generating random numbers from a given
distribution. The method is based on the fact that the cumulative distribution
function of a random variable is a uniformly distributed random variable. The
method is based on the following formula:
\begin{equation}%
    \label{eq:inversion-method}
    X = F^{-1}(U)
\end{equation}
where \( X \) is a random variable with a given distribution, \( F \) is a
cumulative distribution function of \( X \), \( U \) is a uniformly distributed
random variable and \( F^{-1} \) is an inverse function of \( F \).

During laboratory the task was to generate random numbers from a given
distribution using inversion method. The distribution is described as follows
(see Fig.~\ref{fig:inverse-distribution-function}):
\begin{equation}%
    \label{eq:distribution-function}
    f(x) =
    \begin{cases}
        0 \quad \text{for} \quad x < -1            \\
        -x \quad \text{for} \quad -1 \leq x \leq 0 \\
        x \quad \text{for} \quad 0 < x \leq 1      \\
        0 \quad \text{for} \quad x > 1
    \end{cases}
\end{equation}
\begin{figure}[!hptb]
    \centering
    \includegraphics[width = \textwidth]{images/inverse-method/distribution.eps}
    \caption{Distribution function.}%
    \label{fig:inverse-distribution-function}
\end{figure}
The first step was to find a cumulative distribution function of a given
distribution. It was done by calculating the integrals of both subsets and
utilizing the fact that the \( F(0) = 0.5 \).  The cumulative distribution
function is described as follows (see
Fig.~\ref{fig:cumulative-distribution-function}):
\begin{figure}[!hptb]
    \centering
    \includegraphics[width = \textwidth]{images/inverse-method/cumulative-distribution.eps}
    \caption{Cumulative distribution function.}%
    \label{fig:cumulative-distribution-function}
\end{figure}
\begin{equation}%
    \label{eq:distribution-function-integral}
    F(x) =
    \begin{cases}
        0 \quad \text{for} \quad x < -1                                       \\
        -\frac{1}{2}x^2 + \frac{1}{2} \quad \text{for} \quad -1 \leq x \leq 0 \\
        \frac{1}{2}x^2 + \frac{1}{2} \quad \text{for} \quad 0 < x \leq 1      \\
        1 \quad \text{for} \quad x > 1
    \end{cases}
\end{equation}
The second step was to find an inverse function of the cumulative distribution
function. The inverse function is described as follows
\begin{equation}%
    \label{eq:inverse-distribution-function}
    F^{-1}(x) =
    \begin{cases}
        -\sqrt{2x + 1} \quad \text{for} \quad 0 \leq x \leq \frac{1}{2} \\
        \sqrt{2x - 1} \quad \text{for} \quad \frac{1}{2} < x \leq 1
    \end{cases}
\end{equation}
Using the inverse function, the random numbers can be generated using uniform
distribution. The generated random numbers are then supplied to the inverse
function. The histogram of 1000 generated random numbers is shown in
Fig.~\ref{fig:inverse-histogram}.
\begin{figure}[!hptb]
    \centering
    \includegraphics[width = \textwidth]{images/inverse-method/histogram.eps}
    \caption{Histogram of 1000 randomly generated numbers using inverse method.}%
    \label{fig:inverse-histogram}
\end{figure}
Whole code for number generation as well as charts plotting is shown in
Listing~\ref{lst:inversion}.

Based on the results obtained in the experiment it can be said that the
generated numbers follows the original distribution. The algorithm for
generating numbers is straightforward and efficient.

\subsection{Acceptance-rejection method}

The acceptance-rejection method is a method for generating random numbers from a
given distribution. During laboratory the task was to generate random numbers
from a given distribution using acceptance-rejection method. The distribution is
described as follows (see Fig.~\ref{fig:rejection-applicability} for reference):
\begin{equation}%
    \label{eq:gaussian-distribution-function}
    f(x) = \frac{1}{\sqrt{2 \pi}} e^{-\frac{x^2}{2}}
\end{equation}
The general form of the \( g \) function is described as follows:
\begin{equation}%
    \label{eq:exponential-distribution-function-coeficient}
    g(x) = \frac{1}{2} a \cdot e^{-a|x|}.
\end{equation}
For simplicity the \( a \) coefficient is set to \( 1 \), so the \( g \)
function is described as follows (see Fig.~\ref{fig:rejection-applicability} for
reference):
\begin{equation}%
    \label{eq:exponential-distribution-function}
    g(x) = \frac{1}{2} e^{-|x|}.
\end{equation}

\subsubsection{Condition of applicability}

The condition of applicability is described as follows:
\begin{equation}%
    \label{eq:condition-applicability}
    f(x) \leq c \cdot g(x).
\end{equation}
Solving this equation for \( c \) gives the following result:
\begin{equation}%
    \label{eq:condition-applicability-solved}
    \frac{1}{\sqrt{2 \pi}} e^{-\frac{x^2}{2}} \leq c \cdot \frac{1}{2} e^{-|x|}
    \quad \Rightarrow \quad
    c = \sqrt{\frac{2e}{\pi}}.
\end{equation}
The condition of applicability is shown in
Fig.~\ref{fig:rejection-applicability}.
\begin{figure}[!hptb]
    \centering
    \includegraphics[width = \textwidth]{images/rejection-method/distribution.eps}
    \caption{Condition of applicability.}%
    \label{fig:rejection-applicability}
\end{figure}

\subsubsection{Condition of acceptance}

The condition of acceptance is described as follows:
\begin{equation}%
    \label{eq:condition-acceptance}
    {(|x| - 1)}^2 \leq -2 \ln u.
\end{equation}
Using this condition, the random numbers can be generated using uniform
distribution. The generated random numbers are then supplied to the condition of
acceptance. Based on that the numbers are accepted or rejected. The histogram of
1000 generated random numbers is shown in Fig.~\ref{fig:rejection-histogram}.
\begin{figure}[!hptb]
    \centering
    \includegraphics[width = \textwidth]{images/rejection-method/histogram.eps}
    \caption{Histogram of 10000 randomly generated numbers using rejection method.}%
    \label{fig:rejection-histogram}
\end{figure}
Full code for number generation as well as charts plotting is shown in
Listing~\ref{lst:acceptance-rejection}.

As presented in the experiment (Fig.~\ref{fig:rejection-histogram}) the
generated numbers follows the desired distribution. Based on that it can be
concluded that the method is correctly implemented and results are appropriate.

\section{Estimation}

Estimation involves determining the best guess or approximation for an unknown
parameter or variable of interest using observed data. One common method for
parameter estimation is the sample mean, represented as \( \hat{\theta}_N \)
which is calculated as the average of a sample of observations. The formula for
calculating the sample mean is as follows:
\begin{equation}
    \hat{\theta}_N = \frac{1}{N} \sum_{k = 1}^{N} \theta_k
\end{equation}
The sample mean is an unbiased estimator of the population mean, which means
that the expected value of the sample mean is equal to the population mean. The
formula for calculating the expected value of the sample mean is as follows:
\begin{equation}%
    \label{eq:expected-value-sample}
    E \theta_k = E(\theta^* + Z_k) = \theta^* + E(Z_k) = \theta^*
\end{equation}
where \( \theta^* \) is a population mean (scalar) and \( Z_k \) is a random
variable with a mean equal to zero. The estimated value of the sample mean is
equal to the population mean. Using the equation~\ref{eq:expected-value-sample}
the estimated value of the sample mean can be calculated as follows:
\begin{equation}
    E \hat{\theta}_N = \frac{1}{N} \sum_{k = 1}^{N} E \theta_k = \frac{1}{N} \cdot N \cdot \theta^* = \theta^*
\end{equation}
The mean squared error can be calculated as follows:
\begin{equation}
    MSE_N = \frac{1}{R} \sum_{r = 1}^{R} {(\hat{\theta}_N^{(r)} - \theta^*)}^2
\end{equation}
The variance of the random variable \( Z_k \) is equal to \( \frac{1}{12} \) and
is calculated using the equation:
\begin{equation}%
    \label{eq:variance-random-variable}
    var Z = E{(Z - E(Z))}^2 = E{(Z)}^2 = \int_{-\frac{1}{2}}^{\frac{1}{2}} Z^2 \cdot f(Z) dZ = \frac{1}{12}
\end{equation}
Using the equation~\ref{eq:variance-random-variable} the variance of the sample
mean can be calculated as follows:
\begin{equation}
    var \hat{\theta}_N =
    E{(\hat{\theta}_N - E(\hat{\theta}_N))}^2 =
    var ( \frac{1}{N} \sum_{k = 1}^{N} \theta_k ) =
    \frac{1}{N^2} \sum_{k = 1}^{N} var \theta_k =
    \frac{1}{N^2} \cdot N \cdot var Z = \frac{var Z}{N} =
    \frac{1}{12N}
\end{equation}
Comparison of the theoretical and experimental mean squared error is shown in
figure~\ref{fig:estimation}.
\begin{figure}[!hptb]
    \centering
    \begin{subfigure}[b]{0.45\textwidth}
        \centering
        \includegraphics[width=\textwidth]{images/estimation/estimation_r_10.eps}
        \caption{Number of realization \( R = 10 \).}%
        \label{fig:estimation-r-10}
    \end{subfigure}
    \hfill
    \begin{subfigure}[b]{0.45\textwidth}
        \centering
        \includegraphics[width=\textwidth]{images/estimation/estimation_r_50.eps}
        \caption{Number of realization \( R = 50 \).}%
        \label{fig:estimation-r-50}
    \end{subfigure}
    \hfill
    \begin{subfigure}[b]{0.45\textwidth}
        \centering
        \includegraphics[width=\textwidth]{images/estimation/estimation_r_100.eps}
        \caption{Number of realization \( R = 100 \).}%
        \label{fig:estimation-r-100}
    \end{subfigure}
    \hfill
    \begin{subfigure}[b]{0.45\textwidth}
        \centering
        \includegraphics[width=\textwidth]{images/estimation/estimation_r_200.eps}
        \caption{Number of realization \( R = 200 \).}%
        \label{fig:estimation-r-200}
    \end{subfigure}
    \caption{Estimation.}%
    \label{fig:estimation}
\end{figure}
Full code for estimation as well as charts plotting is shown in
Listing~\ref{lst:estimation}.

Based on the results obtained in the experiment it can be said that the
theoretical and experimental mean squared error are similar. As the number of
realization increases the estimated value is closer to the theoretical value.

\pagebreak
\section*{Appendix --- MATLAB codes}

\begin{listing}[!hptb]
    \inputminted{matlab}{data/lab1.m}
    \caption{Inversion method.}%
    \label{lst:inversion}
\end{listing}

\begin{listing}[!hptb]
    \inputminted{matlab}{data/lab2.m}
    \caption{Acceptance-rejection method.}%
    \label{lst:acceptance-rejection}
\end{listing}

\begin{listing}[!hptb]
    \inputminted{matlab}{data/lab3.m}
    \caption{Estimation.}%
    \label{lst:estimation}
\end{listing}

\end{document}
