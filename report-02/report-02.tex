\documentclass[11pt, a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage[left = 2cm,right = 2cm,top = 2cm,bottom = 3cm]{geometry}

\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{float}
\usepackage{graphicx}
\usepackage{multicol}
\usepackage{multirow}
\usepackage{units}
\usepackage{mwe}
\usepackage{graphbox}
\usepackage{verbatim}
\usepackage{fancyvrb}
\usepackage{array}
\usepackage{pdfpages}
\usepackage{booktabs}
\usepackage{setspace}
\usepackage{flafter}
\usepackage{minted}
\usepackage{caption}
\usepackage{subcaption}

\usepackage{report_pwr}

\setminted[matlab]{
    frame=lines,
    framesep=2mm,
    baselinestretch=1.0,
    fontsize=\footnotesize,
    linenos
}

\title{Report 2}
\lecture{Modeling and Identification}
\term{Wednesday 13:15--15:00}
\author{\foreignlanguage{polish}{Jakub Kozłowicz}, 252865}
\lecturer{\foreignlanguage{polish}{Grzegorz Mzyk}, Ph.D., D.Sc., Assoc. Prof.}
\date{December 2023}

\begin{document}

\maketitle
% \tableofcontents
% \pagebreak

\section{Estimation of Probability Density Function}

In the laboratory sessions, we were tasked with the estimation of the
probability density function \( f(u) \) without making any a priori assumptions
regarding its parametric structure, wherein \( u \) denotes a random variable
characterized by \( N \) realizations~\cite{mzyk:2019:nonparametrictoolbox}.

\subsection{Orthogonal Expansion Method}

For the sole method employed in this study, we opted for the orthogonal
expansion technique. The probability density function \( f(u) \) was
approximated utilizing the following expansion
\begin{equation}\label{eq:orthogonal-expansion}
  f(u) \approx \sum_{i=1}^{N} a_i \phi_i(u),
\end{equation}
where \( \phi_i(u) \) denotes orthogonal functions and \( a_i \) represents the
expansion coefficients. These coefficients are derived from experimental data
through sample means
\begin{equation}\label{eq:orthogonal-expansion-coefficients-a}
  \hat{a}_i = \frac{1}{N} \sum_{k=1}^{N} \phi_i(u_k).
\end{equation}
The final estimation of the probability density function is expressed as
\begin{equation}\label{eq:orthogonal-expansion-estimation}
  \hat{f}(u) = \sum_{i=1}^{N} \hat{a}_i \phi_i(u).
\end{equation}
Trigonometric functions were employed as orthogonal functions, and the details
of the \( \phi_i \) function are available in
Listing~\ref{lst:function-f-i}~\cite{mzyk:2019:nonparametrictoolbox}.

\subsubsection{Results}

The outcomes depicted in
Figure~\ref{fig:estimation-probability-density-function} were derived from \( N
= 1000000 \) realizations of the random variable \( u \). Notably, as the
parameter \( S \) experiences an increment, the accuracy of the probability
density function estimation improves. Specifically, the estimation achieves its
maximum accuracy at \( S = 100 \) and its minimum at \( S = 5 \).

\begin{figure}[!hptb]
  \centering
  \begin{subfigure}[b]{0.45\textwidth}
    \includegraphics[width=\textwidth]{images/lab4/5.eps}
    \caption{Estimation of probability density function for \( S = 5 \).}%
    \label{fig:estimation-probability-density-function-5}
  \end{subfigure}
  \hfill
  \begin{subfigure}[b]{0.45\textwidth}
    \includegraphics[width=\textwidth]{images/lab4/10.eps}
    \caption{Estimation of probability density function for \( S = 10 \).}%
    \label{fig:estimation-probability-density-function-10}
  \end{subfigure}
  \medskip
  \begin{subfigure}[b]{0.45\textwidth}
    \includegraphics[width=\textwidth]{images/lab4/20.eps}
    \caption{Estimation of probability density function for \( S = 20 \).}%
    \label{fig:estimation-probability-density-function-20}
  \end{subfigure}
  \hfill
  \begin{subfigure}[b]{0.45\textwidth}
    \includegraphics[width=\textwidth]{images/lab4/50.eps}
    \caption{Estimation of probability density function for \( S = 50 \).}%
    \label{fig:estimation-probability-density-function-50}
  \end{subfigure}
  \medskip
  \begin{subfigure}[b]{0.45\textwidth}
    \includegraphics[width=\textwidth]{images/lab4/100.eps}
    \caption{Estimation of probability density function for \( S = 100 \).}%
    \label{fig:estimation-probability-density-function-100}
  \end{subfigure}
  \caption{Results of estimation of probability density function.}%
  \label{fig:estimation-probability-density-function}
\end{figure}

\section{Estimation of Regression Function}

In the course of the laboratory sessions, we were tasked with the estimation of
nonparametric and nonlinear characteristics of a system exhibiting
noise-corrupted output~\cite{mzyk:2019:nonparametrictoolbox}.

\subsection{Orthogonal Expansion Method}

For the initial approach, we employed the orthogonal expansion method to
approximate the regression function \( \mu(u) \) through the following
expansion
\begin{equation}\label{eq:orthogonal-expansion-regression-function}
  \hat{R}(u) = \frac{\sum_{k=1}^{N} \hat{b}_i \phi_i(u)}{\sum_{k=1}^{N} \hat{a}_i \phi_i(u)},
\end{equation}
where \( \phi_i(u) \) represents orthogonal functions, and \( \hat{a}_i \) and
\( \hat{b}_i \) are the associated expansion coefficients. The \( \hat{a}_i \)
coefficients are determined as per
Eq.~\eqref{eq:orthogonal-expansion-coefficients-a}, while the \( \hat{b}_i \)
coefficients are derived from experimental data, specifically as sample means
\begin{equation}\label{eq:orthogonal-expansion-regression-function-coefficients-b}
  \hat{b}_i = \frac{1}{N} \sum_{k=1}^{N} y_k \phi_i(u_k).
\end{equation}
Trigonometric functions served as the orthogonal functions for this particular
task, alongside the utilization of Legendre
polynomials~\cite{mzyk:2019:nonparametrictoolbox}.

\subsubsection{Results}

The outcomes illustrated in
Figures~\ref{fig:estimation-regression-function-orthogonal}
and~\ref{fig:estimation-regression-function-legendre} were derived from \( N =
1000000 \) realizations of the random variable \( u \). Notably, as the
parameter \( S \) undergoes an increment, the accuracy of the regression
function estimation improves. Specifically, the estimation attains maximal
accuracy at \( S = 100 \) and minimal accuracy at \( S = 5 \). Interestingly,
the results obtained through trigonometric functions demonstrate significantly
higher accuracy compared to those obtained using Legendre polynomials for
smaller values of \( S \). However, as \( S \) increases, the outcomes from both
methods converge, exhibiting substantial similarity.

\begin{figure}[!hptb]
  \centering
  \begin{subfigure}[b]{0.45\textwidth}
    \includegraphics[width=\textwidth]{images/lab5/trygonometric/5.eps}
    \caption{Estimation of regression function for \( S = 5 \).}%
    \label{fig:estimation-regression-function-orthogonal-5}
  \end{subfigure}
  \hfill
  \begin{subfigure}[b]{0.45\textwidth}
    \includegraphics[width=\textwidth]{images/lab5/trygonometric/10.eps}
    \caption{Estimation of regression function for \( S = 10 \).}%
    \label{fig:estimation-regression-function-orthogonal-10}
  \end{subfigure}
  \medskip
  \begin{subfigure}[b]{0.45\textwidth}
    \includegraphics[width=\textwidth]{images/lab5/trygonometric/20.eps}
    \caption{Estimation of regression function for \( S = 20 \).}%
    \label{fig:estimation-regression-function-orthogonal-20}
  \end{subfigure}
  \hfill
  \begin{subfigure}[b]{0.45\textwidth}
    \includegraphics[width=\textwidth]{images/lab5/trygonometric/50.eps}
    \caption{Estimation of regression function for \( S = 50 \).}%
    \label{fig:estimation-regression-function-orthogonal-50}
  \end{subfigure}
  \medskip
  \begin{subfigure}[b]{0.45\textwidth}
    \includegraphics[width=\textwidth]{images/lab5/trygonometric/100.eps}
    \caption{Estimation of regression function for \( S = 100 \).}%
    \label{fig:estimation-regression-function-orthogonal-100}
  \end{subfigure}
  \caption{Results of estimation of regression function using orthogonal method (trigonometric).}%
  \label{fig:estimation-regression-function-orthogonal}
\end{figure}

\begin{figure}[!hptb]
  \centering
  \begin{subfigure}[b]{0.45\textwidth}
    \includegraphics[width=\textwidth]{images/lab5/legendre/5.eps}
    \caption{Estimation of regression function for \( S = 5 \).}%
    \label{fig:estimation-regression-function-legendre-5}
  \end{subfigure}
  \hfill
  \begin{subfigure}[b]{0.45\textwidth}
    \includegraphics[width=\textwidth]{images/lab5/legendre/10.eps}
    \caption{Estimation of regression function for \( S = 10 \).}%
    \label{fig:estimation-regression-function-legendre-10}
  \end{subfigure}
  \medskip
  \begin{subfigure}[b]{0.45\textwidth}
    \includegraphics[width=\textwidth]{images/lab5/legendre/20.eps}
    \caption{Estimation of regression function for \( S = 20 \).}%
    \label{fig:estimation-regression-function-legendre-20}
  \end{subfigure}
  \hfill
  \begin{subfigure}[b]{0.45\textwidth}
    \includegraphics[width=\textwidth]{images/lab5/legendre/50.eps}
    \caption{Estimation of regression function for \( S = 50 \).}%
    \label{fig:estimation-regression-function-legendre-50}
  \end{subfigure}
  \medskip
  \begin{subfigure}[b]{0.45\textwidth}
    \includegraphics[width=\textwidth]{images/lab5/legendre/100.eps}
    \caption{Estimation of regression function for \( S = 100 \).}%
    \label{fig:estimation-regression-function-legendre-100}
  \end{subfigure}
  \caption{Results of estimation of regression function using orthogonal method (Legendre polynomials).}%
  \label{fig:estimation-regression-function-legendre}
\end{figure}

\pagebreak
\subsection{Kernel Method}

For the second methodology, the kernel method was employed to approximate the
regression function \( \mu(u) \) through the following expansion
\begin{equation}\label{eq:kernel-regression-function}
  \hat{R}(u) = \frac{\sum_{k=1}^{N} y_k K\left(\frac{u_k - u}{h}\right)}{\sum_{k=1}^{N} K\left(\frac{u_k - u}{h}\right)},
\end{equation}
where \( K\left(\frac{u_k - u}{h}\right) \) represents the kernel function. The
specific kernel function used for this task is detailed in
Listing~\ref{lst:function-kernel}. The value of \( y_k \) is expressed as
follows
\begin{equation}\label{eq:kernel-regression-function-y}
  y_k = v_k(u_k) + z_k,
\end{equation}
where \( z_k \) denotes the noise with \( EZ = 0 \), and \( v_k \) is defined
recursively as
\begin{equation}\label{eq:kernel-regression-function-v}
  v_k = \frac{1}{2} v_{k - 1} + w_k,
\end{equation}
with the initial value \( v_0 \) set to \( 1 \). The term \( w_k \) is given by
\begin{equation}\label{eq:kernel-regression-function-w}
  w_k = 1 - u_k^2,
\end{equation}
where \( u_k \) represents a random variable with \( N \)
realizations~\cite{mzyk:2019:nonparametrictoolbox}.

\subsubsection{Results}

The results depicted in Figure~\ref{fig:estimation-regression-function-kernel}
were acquired from \( N = 1000000 \) realizations of the random variable \( u
\). Notably, as the parameter \( H \) undergoes an increase, the accuracy of the
regression function estimation improves. However, beyond \( H = 0.1 \), the
accuracy of the estimation diminishes. Optimal accuracy is achieved at \( H =
0.1 \). Noteworthy trends emerge as well; for a smaller window of the kernel
function, the estimation exhibits reduced smoothness, while for a larger window,
the estimation is characterized by enhanced smoothness.

\begin{figure}[!hptb]
  \centering
  \begin{subfigure}[b]{0.45\textwidth}
    \includegraphics[width=\textwidth]{images/lab6/0.0001.eps}
    \caption{Estimation of regression function for \( H = 0.0001 \).}%
    \label{fig:estimation-regression-function-kernel-0.0001}
  \end{subfigure}
  \hfill
  \begin{subfigure}[b]{0.45\textwidth}
    \includegraphics[width=\textwidth]{images/lab6/0.001.eps}
    \caption{Estimation of regression function for \( H = 0.001 \).}%
    \label{fig:estimation-regression-function-kernel-0.001}
  \end{subfigure}
  \medskip
  \begin{subfigure}[b]{0.45\textwidth}
    \includegraphics[width=\textwidth]{images/lab6/0.01.eps}
    \caption{Estimation of regression function for \( H = 0.01 \).}%
    \label{fig:estimation-regression-function-kernel-0.01}
  \end{subfigure}
  \hfill
  \begin{subfigure}[b]{0.45\textwidth}
    \includegraphics[width=\textwidth]{images/lab6/0.1.eps}
    \caption{Estimation of regression function for \( H = 0.1 \).}%
    \label{fig:estimation-regression-function-kernel-0.1}
  \end{subfigure}
  \medskip
  \begin{subfigure}[b]{0.45\textwidth}
    \includegraphics[width=\textwidth]{images/lab6/0.5.eps}
    \caption{Estimation of regression function for \( H = 0.5 \).}%
    \label{fig:estimation-regression-function-kernel-0.5}
  \end{subfigure}
  \hfill
  \begin{subfigure}[b]{0.45\textwidth}
    \includegraphics[width=\textwidth]{images/lab6/1.eps}
    \caption{Estimation of regression function for \( H = 1 \).}%
    \label{fig:estimation-regression-function-kernel-1}
  \end{subfigure}
  \caption{Results of estimation of regression function using orthogonal method (Legendre polynomials).}%
  \label{fig:estimation-regression-function-kernel}
\end{figure}

\pagebreak
\bibliographystyle{plain}
\bibliography{bibliography}

\pagebreak
\section*{Appendix --- MATLAB codes}

\begin{listing}[!hptb]
  \inputminted{matlab}{data/f_i.m}
  \caption{Orthogonal functions based on trigonometry.}%
  \label{lst:function-f-i}
\end{listing}

\begin{listing}[!hptb]
  \inputminted{matlab}{data/mu_u.m}
  \caption{Characteristic of static system.}%
  \label{lst:function-mu-u}
\end{listing}

\begin{listing}[!hptb]
  \inputminted{matlab}{data/lab4.m}
  \caption{Estimation of probability density function.}%
  \label{lst:lab4}
\end{listing}

\begin{listing}[!hptb]
  \inputminted{matlab}{data/lab5.m}
  \caption{Estimation of regression function using orthogonal expansion method.}%
  \label{lst:lab5}
\end{listing}

\begin{listing}[!hptb]
  \inputminted{matlab}{data/kernel.m}
  \caption{Kernel function for estimation of regression function.}%
  \label{lst:function-kernel}
\end{listing}

\begin{listing}[!hptb]
  \inputminted{matlab}{data/lab6.m}
  \caption{Estimation of regression function using kernel method.}%
  \label{lst:lab6}
\end{listing}

\end{document}
