close all;
clear;

S = 100;
N = 1000000;

U = linspace(-1, 1, 100);
U_k = zeros(N, 1);
R_hat = zeros(length(U), 1);
R = zeros(length(U), 1);

for k = 1:N
    U_k(k) = 2 * rand() - 1;
end

a_i = zeros(N, 1);
b_i = zeros(N, 1);

a_hat = zeros(S, 1);
b_hat = zeros(S, 1);

P = legendre(S, U_k, "norm");

for s = 1:S
    for k = 1:N
        a_i(k) = f_i(s, U_k(k)); % a_i(k) = P(s+1, k);
        y_k = mu_u(U_k(k)) + rand() - 0.5;
        b_i(k) = y_k * f_i(s, U_k(k)); % b_i(k) = y_k * P(s+1, k);
    end
    a_hat(s) = sum(a_i) / N;
    b_hat(s) = sum(b_i) / N;
end

P = legendre(S, U, "norm");

for i = 1:length(U)
    a_i = zeros(S, 1);
    b_i = zeros(S, 1);
    for s = 1:S
        a_i(s) = a_hat(s) * f_i(s, U(i)); % a_i(s) = a_hat(s) * P(s+1, i);
        b_i(s) = b_hat(s) * f_i(s, U(i)); % b_i(s) = b_hat(s) * P(s+1, i);
    end
    R_hat(i) = sum(b_i) / sum(a_i);
end

for i = 1:length(U)
    R(i) = mu_u(U(i));
end

figure;
grid on;
hold on;
grid minor;
plot(U, R, "DisplayName", "Real", "LineWidth", 1.5);
plot(U, R_hat, "DisplayName", "Estimated", "LineWidth", 1, "Marker", ".");
legend;
xlim([-1 1])
ylim([-1 1])
