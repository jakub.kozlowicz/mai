close all;
clear;

S = 100;
N = 1000000;

X = linspace(-1, 1, 100);
X_k = zeros(N, 1);
F = zeros(length(X), 1);
F_hat = zeros(length(X), 1);

for k = 1:N
    X_k(k) = rand() + rand() - 1;
end

a_i = zeros(N, 1);

a_hat = zeros(S, 1);

for s = 1:S
    for k = 1:N
        a_i(k) = f_i(s, X_k(k));
    end
    a_hat(s) = sum(a_i) / N;
end

for x = 1:length(X)
    a_i = zeros(S, 1);
    for s = 1:S
        a_i(s) = a_hat(s) * f_i(s, X(x));
    end
    F_hat(x) = sum(a_i);
end

for x = 1:length(X)
    if X(x) < 0
        F(x) = X(x) + 1;
    else
        F(x) = -X(x) + 1;
    end
end

figure;
grid on;
hold on;
grid minor;
plot(X, F, "DisplayName", "Real", "LineWidth", 1.5);
plot(X, F_hat, "DisplayName", "Estimated", "LineWidth", 1, "Marker", ".");
legend;
xlim([-1 1])
ylim([0 1])
