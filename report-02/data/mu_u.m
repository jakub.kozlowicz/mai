function [output] = mu_u(u)
%mu_u Summary of this function goes here
%   Detailed explanation goes here

% [-1, -0.5) -> -2x-2
% [-0.5, 0.5) -> 2x
% [0.5, 1] -> -2x+2

if u >= -1 && u < -0.5
    output = -2*u-2;
elseif u >= -0.5 && u < 0.5
    output = 2*u;
else
    output = -2*u+2;
end

end