function [value] = f_i(i, x)
%F_I Summary of this function goes here
%   Detailed explanation goes here

if i == 1
    value = 1/sqrt(2);
elseif mod(i, 2) == 0
    value = sin(fix(i/2) * pi * x);
else
    value = cos(fix(i/2) * pi * x);
end
end
