close all;
clear;

% Check in report how the bandwith influence the quality.

N = 1000000;
H = 1;

U = linspace(-1, 1, 100);
R = zeros(length(U), 1);
R_est = zeros(length(U), 1);

U_k = zeros(N, 1);
W_k = zeros(N, 1);
V_k = ones(N, 1);
Y_k = zeros(N, 1);

for k = 1:N
    U_k(k) = 2 * rand() - 1;
end

for k = 1:N
    W_k(k) = 1 - U_k(k)^2;
end

for k = 2:N
    V_k(k) = 0.5 * V_k(k - 1) + W_k(k);
end

for k = 1:N
    Y_k(k) = V_k(k) + rand() - 0.5;
end

for i = 1:length(U)
    up = zeros(N, 1);
    down = zeros(N, 1);
    for k = 1:N
        kernel_value = kernel(U_k(k), U(i), H);
        up(k) = Y_k(k) * kernel_value;
        down(k) = kernel_value;
    end
    R_est(i) = sum(up) / sum(down);
end

for i = 1:length(U)
    R(i) = 1 - U(i)^2;
end

figure;
hold on;
grid on;
grid minor;
plot(U, R, "DisplayName", "Real", "LineWidth", 1.5);
plot(U, R_est, "DisplayName", "Estimated", "LineWidth", 1, "Marker", ".");
legend;