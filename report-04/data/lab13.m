close all;
clear;
clc;

N = linspace(100, 10000, 100);
R = 100;

medians = zeros(R, 1);
variances = zeros(length(N), 1);

for i = 1:length(N)
    for r = 1:R
        z_k = randn(N(i), 1);
        medians(r) = median(z_k);
    end
    variances(i) = sum(medians.^2) / (R - 1);
end

figure;
hold on;
grid on;
scatter(N, variances, Marker="*", LineWidth=1.5, DisplayName="Median");
scatter(N, 1./N, Marker="+", LineWidth=1.5, DisplayName="Theoretical");
legend;

