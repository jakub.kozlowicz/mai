clear;
close all;
clc;

N = 1000000;

%% Monte carlo single
X_k = randn(N, 1);

successes = 0;
for k = 1:N
    value = X_k(k)^2 + 2 * X_k(k) - 1;
    if value > 0
        successes = successes + 1;
    end
end

P = successes / N;

disp("Number of samples: " + N);
disp("Result for single MC: " + P);

%% Monte carlo triple
X_k = randn(N, 3);

successes = 0;
for k = 1:N
    value = X_k(k, 1)^2 + X_k(k, 2)^2 +X_k(k, 3)^2;
    if value > 1
        successes = successes + 1;
    end
end

P = successes / N;

disp("Number of samples: " + N);
disp("Result for triple MC: " + P);

%% Bootstrap
X_k = [
    0.5377;
    -1.3499;
    0.6715;
    0.8884;
    -0.1022;
    -0.8637;
    -1.0891;
    -0.6156;
    1.4193;
    0.7254
];

successes = 0;
for k = 1:N
    idxs = randi([1, 10], 3, 1);
    value = X_k(idxs(1))^2 + X_k(idxs(2))^2 +X_k(idxs(3))^2;
    if value > 1
        successes = successes + 1;
    end
end

P = successes / N;

disp("Number of samples: " + N);
disp("Result for bootstrap: " + P);

%% Dynamical system

U_k = rand(N, 1);
H = 2;
T = linspace(0, 3, 100);
P = zeros(length(T), 1);

for i = 1:length(T)
    successes = 0;
    for k = H+1:N
        value = 0;
        for h = 0:H
            value = value + U_k(k - h)^2;
        end
        if value < T(i)
            successes = successes + 1;
        end
    end

    P(i) = successes / N;
end

figure;
hold on;
grid on;
plot(T, P, DisplayName="probability", LineWidth=1.5);
xlabel("Value of T");
ylabel("Value of Probability");
legend;



