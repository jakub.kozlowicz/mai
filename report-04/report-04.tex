\documentclass[11pt, a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage[left = 2cm,right = 2cm,top = 2cm,bottom = 3cm]{geometry}

\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{float}
\usepackage{graphicx}
\usepackage{multicol}
\usepackage{multirow}
\usepackage{units}
\usepackage{mwe}
\usepackage{graphbox}
\usepackage{verbatim}
\usepackage{fancyvrb}
\usepackage{array}
\usepackage{pdfpages}
\usepackage{booktabs}
\usepackage{setspace}
\usepackage{flafter}
\usepackage{minted}
\usepackage{caption}
\usepackage{subcaption}

\usepackage{report_pwr}

\setminted[matlab]{
    frame=lines,
    framesep=2mm,
    baselinestretch=1.0,
    fontsize=\footnotesize,
    linenos
}

\title{Report 4}
\lecture{Modeling and Identification}
\term{Wednesday 13:15--15:00}
\author{\foreignlanguage{polish}{Jakub Kozłowicz}, 252865}
\lecturer{\foreignlanguage{polish}{Grzegorz Mzyk}, Ph.D., D.Sc., Assoc. Prof.}
\date{January 2024}

\begin{document}

\maketitle
% \tableofcontents
% \pagebreak

\section{Monte Carlo and Bootstrap methods}

During laboratory classes, the Monte Carlo and Bootstrap methods were used to
estimate different probabilities.

\subsection{Monte Carlo method}

First, the Monte Carlo method was used to estimate the probability of
\begin{equation}
    P( x^2 + 2x - 1 > 0)
\end{equation}
being true for \( x \) being a random variable with a normal distribution
\( N(0, 1) \). The output of the Monte Carlo method for
this task is \textbf{0.3470}.

The Monte Carlo method was also used to estimate the probability of
\begin{equation}
    P( \sum_{i=1}^{3} {x_i}^2 > 1)
\end{equation}
being true for \( x \) being a random variable with a normal distribution (\(
N(0, 1) \)). The three values of \( x_i \) were independent. The output of the
Monte Carlo method for this task is \textbf{0.8018}.

\subsection{Bootstrap method}

The Bootstrap method was used to estimate the probability of
\begin{equation}
    P( \sum_{i=1}^{3} {x_i}^2 > 1)
\end{equation}
being true for \( x \) being a variable from given set of unknown distribution.
The set of values is given by:
\begin{equation}
    \{ 0.5377, -1.3499, 0.6715, 0.8884, -0.1022, -0.8637, -1.0891, -0.6156, 1.4193, 0.7254 \}.
\end{equation}
Important note is that the values can be repeated. The output of the Bootstrap
method for this task is \textbf{0.9318}.

The last estimation with the Bootstrap method was the probability of
\begin{equation}
    P( y_k < T)
\end{equation}
being true for \( y \) defined as:
\begin{equation}
    y_k = \sum_{i = 0}^{2} u_{k - i},
\end{equation}
where \( u \) is a random variable with a uniform distribution on the interval
\( [0, 1] \). The output of the Bootstrap method for this task is shown in
Figure~\ref{fig:bootstrap}.
\begin{figure}[!hptb]
    \centering
    \includegraphics[width=0.8\textwidth]{images/bootstrap.eps}
    \caption{Estimation using the Bootstrap method.}%
    \label{fig:bootstrap}
\end{figure}

The results of the estimation shows that the probability of \( y_k < T \) being
true increases with the increase of \( T \). The value of probability
above \( 0.5 \) is reached for \( T \approx 1.0 \).

\section{Recursive Least Squares Methods and Instrumental Variables}

During laboratory classes, the Recursive Least Squares and Instrumental
Variables methods were used to estimate the following parameters
\begin{equation}
    \begin{aligned}
        \theta_1 & = 0.5, \\
        \theta_2 & = 0.3, \\
        \theta_3 & = 1.0,
    \end{aligned}
\end{equation}
The system was described by the following equation
\begin{equation}
    y_k = v_k + z_k,
\end{equation}
where
\begin{equation}
    \begin{aligned}
        v_k & = \theta_1 v_{k - 1} + \theta_2 v_{k - 2} + \theta_3 u_{k}, \\
        z_k & = 0.5 z_{k - 1} + \epsilon_k,
    \end{aligned}
\end{equation}
where \( \epsilon_k \) is a random variable with a uniform distribution on the
interval \( [-0.1, 0.1] \) and \( u_k \) is a random variable with a uniform
distribution on the interval \( [-1, 1] \).

The equation for the Recursive Least Squares method estimation is given by
\begin{equation}
    \hat{\theta}_k = \hat{\theta}_{k - 1} + P_k \phi_k \left( y_k - \phi_k^T \hat{\theta}_{k - 1} \right),
\end{equation}
where
\begin{equation}
    \begin{aligned}
        \phi_k & = \begin{bmatrix} y_{k - 1} & y_{k - 2} & u_k \end{bmatrix}^T,                                             \\
        P_k    & = P_{k - 1} - \left( \frac{P_{k - 1} \phi_k {\phi_k}^T P_{k - 1}}{1 + {\phi_k}^T P_{k - 1} \phi_k} \right)
    \end{aligned}
\end{equation}
Initial conditions for the Recursive Least Squares method are given by
\begin{equation}
    \begin{aligned}
        \hat{\theta}_0 & = \begin{bmatrix} 0 & 0 & 0 \end{bmatrix}^T,                               \\
        P_0            & = \begin{bmatrix} 100 & 0 & 0 \\ 0 & 100 & 0 \\ 0 & 0 & 100 \end{bmatrix}.
    \end{aligned}
\end{equation}

The estimated parameters from the Recursive Least Squares method were used to
estimate the parameters of the system using the Instrumental Variables method.
The equation for the model is given by
\begin{equation}
    \bar{y}_k = \hat{\theta}_1 \bar{y}_{k - 1} + \hat{\theta}_2 \bar{y}_{k - 2} + \hat{\theta}_3 u_k.
\end{equation}
The equation for the Instrumental Variables method is given by
\begin{equation}
    {\hat{\theta}_k}^{i.v} = {\hat{\theta}_{k - 1}}^{i.v} + {P_k}^{i.v} \psi_k \left( y_k - {{\phi_k}^{i.v}}^T {\hat{\theta}_{k - 1}}^{i.v} \right),
\end{equation}
where
\begin{equation}
    \begin{aligned}
        \phi_k      & = \begin{bmatrix} y_{k - 1} & y_{k - 2} & u_k \end{bmatrix}^T,                                                                             \\
        \psi_k      & = \begin{bmatrix} \bar{y}_{k - 1} & \bar{y}_{k - 2} & u_k \end{bmatrix}^T,                                                                 \\
        {P_k}^{i.v} & = {P_{k - 1}}^{i.v} - \left( \frac{{P_{k - 1}}^{i.v} \psi_k {\phi_k}^T {P_{k - 1}}^{i.v}}{1 + {\phi_k}^T {P_{k - 1}}^{i.v} \psi_k} \right)
    \end{aligned}
\end{equation}
Initial conditions for the Instrumental Variables method are given by
\begin{equation}
    \begin{aligned}
        {\hat{\theta}_0}^{i.v} & = \begin{bmatrix} 0 & 0 & 0 \end{bmatrix}^T,                               \\
        {P_0}^{i.v}            & = \begin{bmatrix} 100 & 0 & 0 \\ 0 & 100 & 0 \\ 0 & 0 & 100 \end{bmatrix}.
    \end{aligned}
\end{equation}

The results of the estimation are shown in Figure~\ref{fig:iv-ls-comparison}.
\begin{figure}[!hptb]
    \centering
    \includegraphics[width=0.8\textwidth]{images/iv-ls-comparison.eps}
    \caption{Error comparison between Instrumental Variables and Recursive Least Squares methods.}%
    \label{fig:iv-ls-comparison}
\end{figure}
The results show that the Instrumental Variables method is more accurate than
the Recursive Least Squares method. The error of the Instrumental Variables
goes to zero, while the error of the Recursive Least Squares method is
biased.

\end{document}
