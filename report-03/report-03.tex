\documentclass[11pt, a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage[left = 2cm,right = 2cm,top = 2cm,bottom = 3cm]{geometry}

\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{float}
\usepackage{graphicx}
\usepackage{multicol}
\usepackage{multirow}
\usepackage{units}
\usepackage{mwe}
\usepackage{graphbox}
\usepackage{verbatim}
\usepackage{fancyvrb}
\usepackage{array}
\usepackage{pdfpages}
\usepackage{booktabs}
\usepackage{setspace}
\usepackage{flafter}
\usepackage{minted}
\usepackage{caption}
\usepackage{subcaption}

\usepackage{report_pwr}

\setminted[matlab]{
    frame=lines,
    framesep=2mm,
    baselinestretch=1.0,
    fontsize=\footnotesize,
    linenos
}

\title{Report 3}
\lecture{Modeling and Identification}
\term{Wednesday 13:15--15:00}
\author{\foreignlanguage{polish}{Jakub Kozłowicz}, 252865}
\lecturer{\foreignlanguage{polish}{Grzegorz Mzyk}, Ph.D., D.Sc., Assoc. Prof.}
\date{December 2023}

\begin{document}

\maketitle
% \tableofcontents
% \pagebreak

\section{Parametric Identification}

During the experimental sessions in the laboratory, the parameters of the
ensuing system were assessed:
\begin{equation}
    \label{eq:lab7-system}
    y_k = b_0 u_k + b_1 u_{k-1} + b_2 u_{k-2} + b_3 u_{k-3} + z_k,
\end{equation}
where \( z_k \) denotes a noise variable with an expected value of 0. The
formula for estimating the parameters is expressed as:
\begin{equation}
    \label{eq:lab7-parameters}
    \hat{b}_k = \hat{b}_{k-1} + P_k \phi_k \left( y_k - \phi_k^T \hat{b}_{k-1} \right),
\end{equation}
where \( \hat{b}_k \) is a vector representing the estimated parameters, \( P_k
\) is a matrix indicating the covariance of parameters, \( \phi_k \) is a vector
encompassing the regressors, and \( y_k \) denotes the output value. The initial
values of \( \hat{b}_0 \) and \( P_0 \) are both set to 0. The regressors are
determined as follows:
\begin{equation}
    \label{eq:lab7-regressors}
    \phi_k = \begin{bmatrix}
        u_k     \\
        u_{k-1} \\
        u_{k-2} \\
        u_{k-3}
    \end{bmatrix}.
\end{equation}
The computation of the covariance matrix is articulated as:
\begin{equation}
    \label{eq:lab7-covariance}
    P_k = P_{k-1} - \frac{P_{k-1} \phi_k \phi_k^T P_{k-1}}{1 + \phi_k^T P_{k-1} \phi_k}.
\end{equation}

\subsection{Results}

The estimations of the parameters \( \hat{b} \) for varying numbers of samples
are depicted in Figure~\ref{fig:lab7-estimation}. Corresponding numerical
\begin{figure}[!hptb]
    \centering
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{images/lab7/100.eps}
        \caption{Estimation of parameters for \( N = 100 \).}%
        \label{fig:lab7-estimation-100}
    \end{subfigure}
    \hfill
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{images/lab7/1000.eps}
        \caption{Estimation of parameters for \( N = 1000 \).}%
        \label{fig:lab7-estimation-1000}
    \end{subfigure}
    \medskip
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{images/lab7/10_000.eps}
        \caption{Estimation of parameters for \( N = 10000 \).}%
        \label{fig:lab7-estimation-10000}
    \end{subfigure}
    \hfill
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{images/lab7/100_000.eps}
        \caption{Estimation of parameters for \( N = 100000 \).}%
        \label{fig:lab7-estimation-100000}
    \end{subfigure}
    \caption{Results of (\( \hat{b} \)) parameter estimation for different sample sizes.}%
    \label{fig:lab7-estimation}
\end{figure}
results are presented in Table~\ref{tab:lab7-b}.
\begin{table}[!hptb]
    \centering
    \caption{Estimation of (\( \hat{b} \)) parameters for different numbers of samples.}%
    \label{tab:lab7-b}
    \begin{subtable}[t]{0.45\textwidth}
        \caption{Estimation of \( \hat{b} \) parameters for \( N = 100 \) samples.}%
        \label{tab:lab7-b-100}
        \begin{tabular}{lccc}
            \toprule
            Parameter       & Real   & Estimated & Error  \\ \midrule
            \( \hat{b}_1 \) & 4.0000 & 3.9853    & 0.0147 \\ \midrule
            \( \hat{b}_2 \) & 3.0000 & 3.0981    & 0.0981 \\ \midrule
            \( \hat{b}_3 \) & 2.0000 & 1.8964    & 0.1036 \\ \midrule
            \( \hat{b}_4 \) & 1.0000 & 1.0295    & 0.0295 \\ \bottomrule
        \end{tabular}
    \end{subtable}
    \hfill
    \begin{subtable}[t]{0.45\textwidth}
        \caption{Estimation of \( \hat{b} \) parameters for \( N = 1000 \) samples.}%
        \label{tab:lab7-b-1000}
        \begin{tabular}{lccc}
            \toprule
            Parameter       & Real   & Estimated & Error  \\ \midrule
            \( \hat{b}_1 \) & 4.0000 & 3.9784    & 0.0216 \\ \midrule
            \( \hat{b}_2 \) & 3.0000 & 3.0381    & 0.0381 \\ \midrule
            \( \hat{b}_3 \) & 2.0000 & 1.9871    & 0.0129 \\ \midrule
            \( \hat{b}_4 \) & 1.0000 & 0.9867    & 0.0133 \\ \bottomrule
        \end{tabular}
    \end{subtable}
    \bigskip
    \begin{subtable}[t]{0.45\textwidth}
        \caption{Estimation of \( \hat{b} \) parameters for \( N = 10000 \) samples.}%
        \label{tab:lab7-b-10000}
        \begin{tabular}{lccc}
            \toprule
            Parameter       & Real   & Estimated & Error  \\ \midrule
            \( \hat{b}_1 \) & 4.0000 & 3.9871    & 0.0129 \\ \midrule
            \( \hat{b}_2 \) & 3.0000 & 2.9881    & 0.0119 \\ \midrule
            \( \hat{b}_3 \) & 2.0000 & 2.0262    & 0.0262 \\ \midrule
            \( \hat{b}_4 \) & 1.0000 & 1.0028    & 0.0028 \\ \bottomrule
        \end{tabular}
    \end{subtable}
    \hfill
    \begin{subtable}[t]{0.45\textwidth}
        \caption{Estimation of \( \hat{b} \) parameters for \( N = 100000 \) samples.}%
        \label{tab:lab7-b-100000}
        \begin{tabular}{lccc}
            \toprule
            Parameter       & Real   & Estimated & Error  \\ \midrule
            \( \hat{b}_1 \) & 4.0000 & 3.9955    & 0.0045 \\ \midrule
            \( \hat{b}_2 \) & 3.0000 & 2.9969    & 0.0031 \\ \midrule
            \( \hat{b}_3 \) & 2.0000 & 2.0009    & 0.0009 \\ \midrule
            \( \hat{b}_4 \) & 1.0000 & 1.0056    & 0.0056 \\ \bottomrule
        \end{tabular}
    \end{subtable}
\end{table}
The tabulated results display the estimated parameter values along with the
error between the real and estimated values. Notably, the error diminishes with
an increasing number of samples, indicating proximity between the estimated and
actual values.

\newpage
\section{Wiener System Identification}

During laboratory experiments, the parameters of the following system were
estimated:
\begin{equation}
    \label{eq:lab8-system}
    y_k = {x_k}^2 + x_k + z_k,
\end{equation}
where \( z_k \) represents noise with an expected value of 0, and \( x_k \) is
defined as:
\begin{equation}
    \label{eq:lab8-x}
    x_k = 3 u_k + 2 u_{k-1} + u_{k-2}.
\end{equation}
Here, \( u_k \) is a random variable with a uniform distribution on the interval
\( \left[ -1, 1 \right] \). The equation for calculating the parameters is
expressed as:
\begin{equation}
    \label{eq:lab8-parameters}
    \hat{\gamma} = {\left( \sum_{k=1}^{N} {\phi_k} {\phi_k}^T K \left( \frac{|| \phi_k ||}{h} \right) \right)}^{-1} \sum_{k=1}^{N} {\phi_k} y_k K \left( \frac{|| \phi_k ||}{h} \right),
\end{equation}
where \( \phi_k \) is defined as:
\begin{equation}
    \label{eq:lab8-regressor}
    \phi_k = \begin{bmatrix}
        u_k     \\
        u_{k-1} \\
        u_{k-2}
    \end{bmatrix}.
\end{equation}

Following the estimation of parameters \( \hat{\gamma} \), the inner signal of
the first block was constructed using the estimated values to subsequently
estimate the nonlinear, static block using the Kernel method. The corresponding
equation is given by:
\begin{equation}
    \hat{R}(u) = \frac{\sum_{k=1}^{N} y_k K\left(\frac{x_k - u}{h}\right)}{\sum_{k=1}^{N} K\left(\frac{x_k - u}{h}\right)},
\end{equation}

\subsection{Results}

The results of estimating the parameters \( \hat{\theta} \) for various sample
sizes are summarized in Table~\ref{tab:lab8-gamma}. As the number of samples
\begin{table}[!hptb]
    \centering
    \caption{Parameter estimation (\( \hat{\gamma} \)) for different sample sizes.}%
    \label{tab:lab8-gamma}
    \begin{subtable}[t]{0.45\textwidth}
        \caption{Parameter estimation for \( N = 100 \) samples.}%
        \label{tab:lab8-gamma-100}
        \begin{tabular}{lccc}
            \toprule
            Parameter            & Real   & Estimated & Error  \\ \midrule
            \( \hat{\gamma}_1 \) & 3.0000 & 3.6985    & 0.6985 \\ \midrule
            \( \hat{\gamma}_2 \) & 2.0000 & 5.3372    & 3.3372 \\ \midrule
            \( \hat{\gamma}_3 \) & 1.0000 & 1.4275    & 0.4275 \\ \midrule
        \end{tabular}
    \end{subtable}
    \hfill
    \begin{subtable}[t]{0.45\textwidth}
        \caption{Parameter estimation for \( N = 1000 \) samples.}%
        \label{tab:lab8-gamma-1000}
        \begin{tabular}{lccc}
            \toprule
            Parameter            & Real   & Estimated & Error  \\ \midrule
            \( \hat{\gamma}_1 \) & 3.0000 & 2.6146    & 1.6146 \\ \midrule
            \( \hat{\gamma}_2 \) & 2.0000 & 1.4998    & 0.5002 \\ \midrule
            \( \hat{\gamma}_3 \) & 1.0000 & 0.5049    & 0.4951 \\ \midrule
        \end{tabular}
    \end{subtable}
    \bigskip
    \begin{subtable}[t]{0.45\textwidth}
        \caption{Parameter estimation for \( N = 10000 \) samples.}%
        \label{tab:lab8-gamma-10000}
        \begin{tabular}{lccc}
            \toprule
            Parameter            & Real   & Estimated & Error  \\ \midrule
            \( \hat{\gamma}_1 \) & 3.0000 & 3.0626    & 0.0626 \\ \midrule
            \( \hat{\gamma}_2 \) & 2.0000 & 2.2270    & 0.2270 \\ \midrule
            \( \hat{\gamma}_3 \) & 1.0000 & 1.1638    & 0.1638 \\ \midrule
        \end{tabular}
    \end{subtable}
    \hfill
    \begin{subtable}[t]{0.45\textwidth}
        \caption{Parameter estimation for \( N = 100000 \) samples.}%
        \label{tab:lab8-gamma-100000}
        \begin{tabular}{lccc}
            \toprule
            Parameter            & Real   & Estimated & Error  \\ \midrule
            \( \hat{\gamma}_1 \) & 3.0000 & 3.0993    & 0.0993 \\ \midrule
            \( \hat{\gamma}_2 \) & 2.0000 & 2.0936    & 0.0936 \\ \midrule
            \( \hat{\gamma}_3 \) & 1.0000 & 0.9947    & 0.0053 \\ \midrule
        \end{tabular}
    \end{subtable}
\end{table}
increases, the error between the real and estimated values decreases. The
estimated values closely approximate the real values. The estimated non-linear
block for different sample sizes is illustrated in
Figure~\ref{fig:lab8-estimation}.
\begin{figure}[!hptb]
    \centering
    \begin{subfigure}[b]{0.45\textwidth}
        \centering
        \includegraphics[width=\textwidth]{images/lab8/100.eps}
        \caption{Estimation of non-linear, static block for \( N = 100 \).}%
        \label{fig:lab8-estimation-100}
    \end{subfigure}
    \hfill
    \begin{subfigure}[b]{0.45\textwidth}
        \centering
        \includegraphics[width=\textwidth]{images/lab8/1000.eps}
        \caption{Estimation of non-linear, static block for \( N = 1000 \).}%
        \label{fig:lab8-estimation-1000}
    \end{subfigure}
    \medskip
    \begin{subfigure}[b]{0.45\textwidth}
        \centering
        \includegraphics[width=\textwidth]{images/lab8/10_000.eps}
        \caption{Estimation of non-linear, static block for \( N = 10000 \).}%
        \label{fig:lab8-estimation-10000}
    \end{subfigure}
    \hfill
    \begin{subfigure}[b]{0.45\textwidth}
        \centering
        \includegraphics[width=\textwidth]{images/lab8/100_000.eps}
        \caption{Estimation of non-linear, static block for \( N = 100000 \).}%
        \label{fig:lab8-estimation-100000}
    \end{subfigure}
    \caption{Estimation of non-linear, static block for different sample sizes.}%
    \label{fig:lab8-estimation}
\end{figure}

\newpage
\section{Hammerstein System Identification}

During the laboratory sessions, the parameters of the following system were
estimated:
\begin{equation}
    \label{eq:lab9-system}
    y_k = \gamma_0^* w_k + \gamma_1^* w_{k-1} + \gamma_2^* w_{k-2} + z_k,
\end{equation}
where \( z_k \) represents noise with an expected value of 0, and \( w_k \) is
defined as:
\begin{equation}
    \label{eq:lab9-w}
    w_k = c_1 \cdot u_k + c_2 \cdot u_k^2.
\end{equation}
Here, the vector \( C \) is equal to \( \begin{bmatrix} 3 \\ 2 \end{bmatrix} \).
The equation for calculating the parameters is given by:
\begin{equation}
    \label{eq:lab9-parameters}
    \hat{\theta} = {\left( {\Phi_N}^T \Phi_N \right)}^{-1} {\Phi_N}^T Y_N,
\end{equation}
where \( \hat{\theta} \) is a vector of estimated parameters, \( \Phi_N \) is a
matrix of regressors, and \( Y_N \) is a vector of output values. The regressors
are calculated as follows:
\begin{equation}
    \label{eq:lab9-regressors}
    \Phi_N = \begin{bmatrix}
        {\phi_1}^T & \rightarrow \\
        {\phi_2}^T & \rightarrow \\
        \vdots     & \rightarrow \\
        {\phi_N}^T & \rightarrow
    \end{bmatrix},
\end{equation}
and \( Y_N \) is calculated as:
\begin{equation}
    \label{eq:lab9-y}
    Y_N = \begin{bmatrix}
        y_1    \\
        y_2    \\
        \vdots \\
        y_N
    \end{bmatrix}.
\end{equation}
The individual regressors are computed as:
\begin{equation}
    \label{eq:lab9-regressor}
    \phi_k = \begin{bmatrix}
        u_k         \\
        {u_k}^2     \\
        u_{k-1}     \\
        {u_{k-1}}^2 \\
        u_{k-2}     \\
        {u_{k-2}}^2
    \end{bmatrix}.
\end{equation}

\subsection{Results}

Results of the estimation of \( \hat{\theta} \) parameters for different numbers
of samples are presented in Table~\ref{tab:lab9-theta}. As the number of samples
\begin{table}[!hptb]
    \centering
    \caption{Estimation of \( \theta \) parameters for different numbers of samples.}%
    \label{tab:lab9-theta}
    \begin{subtable}[t]{0.45\textwidth}
        \caption{Estimation of \( \theta \) parameters for \( N = 100 \) samples.}%
        \label{tab:lab9-theta-100}
        \begin{tabular}{lccc}
            \toprule
            Parameter      & Real   & Estimated & Error  \\ \midrule
            \( \theta_1 \) & 9.0000 & 8.9816    & 0.0184 \\ \midrule
            \( \theta_2 \) & 6.0000 & 5.9717    & 0.0283 \\ \midrule
            \( \theta_3 \) & 6.0000 & 6.0585    & 0.0585 \\ \midrule
            \( \theta_4 \) & 4.0000 & 4.0012    & 0.0012 \\ \midrule
            \( \theta_5 \) & 3.0000 & 2.9399    & 0.0601 \\ \midrule
            \( \theta_6 \) & 2.0000 & 1.9309    & 0.0691 \\ \bottomrule
        \end{tabular}
    \end{subtable}
    \hfill
    \begin{subtable}[t]{0.45\textwidth}
        \caption{Estimation of \( \theta \) parameters for \( N = 1000 \) samples.}%
        \label{tab:lab9-theta-1000}
        \begin{tabular}{lccc}
            \toprule
            Parameter      & Real   & Estimated & Error  \\ \midrule
            \( \theta_1 \) & 9.0000 & 8.9972    & 0.0028 \\ \midrule
            \( \theta_2 \) & 6.0000 & 6.0144    & 0.0144 \\ \midrule
            \( \theta_3 \) & 6.0000 & 6.0123    & 0.0123 \\ \midrule
            \( \theta_4 \) & 4.0000 & 4.0225    & 0.0225 \\ \midrule
            \( \theta_5 \) & 3.0000 & 3.0264    & 0.0264 \\ \midrule
            \( \theta_6 \) & 2.0000 & 2.0167    & 0.0167 \\ \bottomrule
        \end{tabular}
    \end{subtable}
    \bigskip
    \begin{subtable}[t]{0.45\textwidth}
        \caption{Estimation of \( \theta \) parameters for \( N = 10000 \) samples.}%
        \label{tab:lab9-theta-10000}
        \begin{tabular}{lccc}
            \toprule
            Parameter      & Real   & Estimated & Error  \\ \midrule
            \( \theta_1 \) & 9.0000 & 8.9976    & 0.0024 \\ \midrule
            \( \theta_2 \) & 6.0000 & 5.9889    & 0.0111 \\ \midrule
            \( \theta_3 \) & 6.0000 & 6.0047    & 0.0047 \\ \midrule
            \( \theta_4 \) & 4.0000 & 4.0110    & 0.0110 \\ \midrule
            \( \theta_5 \) & 3.0000 & 3.0009    & 0.0009 \\ \midrule
            \( \theta_6 \) & 2.0000 & 1.9961    & 0.0039 \\ \bottomrule
        \end{tabular}
    \end{subtable}
    \hfill
    \begin{subtable}[t]{0.45\textwidth}
        \caption{Estimation of \( \theta \) parameters for \( N = 100000 \) samples.}%
        \label{tab:lab9-theta-100000}
        \begin{tabular}{lccc}
            \toprule
            Parameter      & Real   & Estimated & Error  \\ \midrule
            \( \theta_1 \) & 9.0000 & 8.9979    & 0.0021 \\ \midrule
            \( \theta_2 \) & 6.0000 & 5.9972    & 0.0028 \\ \midrule
            \( \theta_3 \) & 6.0000 & 5.9992    & 0.0008 \\ \midrule
            \( \theta_4 \) & 4.0000 & 3.9973    & 0.0027 \\ \midrule
            \( \theta_5 \) & 3.0000 & 3.0020    & 0.0020 \\ \midrule
            \( \theta_6 \) & 2.0000 & 2.0042    & 0.0042 \\ \bottomrule
        \end{tabular}
    \end{subtable}
\end{table}
increases, the error between the real and estimated values decreases. The
estimated values are close to the real values.

Additionally, using the singular value decomposition (SVD), the parameters \(
\gamma \) and \( C \) were estimated. The results are presented in
Table~\ref{tab:lab9-gamma} and Table~\ref{tab:lab9-c}. The method produces
\begin{table}[!hptb]
    \centering
    \caption{Estimation of \( \gamma \) parameters for all three systems for \( N = 1000000 \) samples.}%
    \label{tab:lab9-gamma}
    \begin{tabular}{lccc}
        \toprule
        Parameter      & Real value & Estimated value & Scale   \\ \midrule
        \( \gamma_1 \) & 3.0000     & -0.8018         & -3.7417 \\ \midrule
        \( \gamma_2 \) & 2.0000     & -0.5345         & -3.7417 \\ \midrule
        \( \gamma_3 \) & 1.0000     & -0.2673         & -3.7417 \\ \bottomrule
    \end{tabular}
\end{table}
\begin{table}[!hptb]
    \centering
    \caption{Estimation of \( C \) parameters for all three systems for \( N = 1000000 \) samples.}%
    \label{tab:lab9-c}
    \begin{tabular}{lccc}
        \toprule
        Parameter & Real value & Estimated value & Scale   \\ \midrule
        \( c_1 \) & 3.0000     & -0.8321         & -3.6056 \\ \midrule
        \( c_2 \) & 2.0000     & -0.5547         & -3.6056 \\ \bottomrule
    \end{tabular}
\end{table}
scaled values of parameters, so the scaling factor was calculated, and the
results are presented in the last column of the tables.

\newpage
\section{Identification with Cross-correlation Approach}

During laboratory classes, the parameters for three systems (linear, Wiener,
Hammerstein) were estimated using the cross-correlation approach. The equation
to calculate the parameters is given by:
\begin{equation}
    \label{eq:lab10-parameters}
    \hat{\gamma}_{\tau} = \frac{1}{N - \tau} \sum_{k=1}^{N - \tau} u_k y_{k+\tau}.
\end{equation}

\subsection{Results}

Results of the estimation of \( \hat{\gamma} \) parameters for different numbers
of samples for all three systems are presented in Table~\ref{tab:lab10-gamma}.
\begin{table}[!hptb]
    \centering
    \caption{Estimation of \( \hat{\gamma} \) parameters for different numbers of samples.}%
    \label{tab:lab10-gamma}
    \begin{subtable}[t]{\textwidth}
        \centering
        \caption{Estimation of \( \hat{\gamma} \) parameters for \( N = 100 \) samples.}%
        \label{tab:lab10-gamma-100}
        \begin{tabular}{lccccccc}
            \toprule
            Parameter            & Real   & Linear  & Error  & Hammerstein & Error  & Wiener  & Error  \\ \midrule
            \( \hat{\gamma}_1 \) & 1.0000 & 1.0000  & 0.0000 & 1.0000      & 0.0000 & 1.0000  & 0.0000 \\ \midrule
            \( \hat{\gamma}_2 \) & 0.5000 & 0.2860  & 0.2140 & 0.0578      & 0.4422 & 0.0686  & 0.4314 \\ \midrule
            \( \hat{\gamma}_3 \) & 0.2500 & 0.1611  & 0.0889 & 0.1319      & 0.1181 & 0.5534  & 0.3034 \\ \midrule
            \( \hat{\gamma}_4 \) & 0.1250 & 0.0691  & 0.0559 & -0.0560     & 0.1810 & -0.2403 & 0.3653 \\ \midrule
            \( \hat{\gamma}_5 \) & 0.0625 & -0.0064 & 0.0689 & -0.3206     & 0.3831 & -0.1859 & 0.2484 \\ \midrule
            \( \hat{\gamma}_6 \) & 0.0312 & -0.1054 & 0.1367 & -0.1505     & 0.1817 & 0.1809  & 0.1497 \\ \bottomrule
        \end{tabular}
    \end{subtable}
    \bigskip
    \begin{subtable}[t]{\textwidth}
        \centering
        \caption{Estimation of \( \hat{\gamma} \) parameters for \( N = 1000 \) samples.}%
        \label{tab:lab10-gamma-1000}
        \begin{tabular}{lccccccc}
            \toprule
            Parameter            & Real   & Linear & Error  & Hammerstein & Error  & Wiener  & Error  \\ \midrule
            \( \hat{\gamma}_1 \) & 1.0000 & 1.0000 & 0.0000 & 1.0000      & 0.0000 & 1.0000  & 0.0000 \\ \midrule
            \( \hat{\gamma}_2 \) & 0.5000 & 0.5012 & 0.0012 & 0.4983      & 0.0017 & 0.5143  & 0.0143 \\ \midrule
            \( \hat{\gamma}_3 \) & 0.2500 & 0.2617 & 0.0117 & 0.1861      & 0.0639 & 0.2639  & 0.0139 \\ \midrule
            \( \hat{\gamma}_4 \) & 0.1250 & 0.1698 & 0.0448 & 0.1413      & 0.0163 & 0.1555  & 0.0305 \\ \midrule
            \( \hat{\gamma}_5 \) & 0.0625 & 0.0657 & 0.0032 & 0.0615      & 0.0010 & 0.1455  & 0.0830 \\ \midrule
            \( \hat{\gamma}_6 \) & 0.0312 & 0.0212 & 0.0101 & -0.0470     & 0.0782 & -0.0344 & 0.0657 \\ \bottomrule
        \end{tabular}
    \end{subtable}
    \bigskip
    \begin{subtable}[t]{\textwidth}
        \centering
        \caption{Estimation of \( \hat{\gamma} \) parameters for \( N = 10000 \) samples.}%
        \label{tab:lab10-gamma-10000}
        \begin{tabular}{lccccccc}
            \toprule
            Parameter            & Real   & Linear & Error  & Hammerstein & Error  & Wiener & Error  \\ \midrule
            \( \hat{\gamma}_1 \) & 1.0000 & 1.0000 & 0.0000 & 1.0000      & 0.0000 & 1.0000 & 0.0000 \\ \midrule
            \( \hat{\gamma}_2 \) & 0.5000 & 0.5226 & 0.0226 & 0.5237      & 0.0237 & 0.5181 & 0.0181 \\ \midrule
            \( \hat{\gamma}_3 \) & 0.2500 & 0.2632 & 0.0132 & 0.2637      & 0.0137 & 0.2616 & 0.0116 \\ \midrule
            \( \hat{\gamma}_4 \) & 0.1250 & 0.1556 & 0.0306 & 0.1413      & 0.0163 & 0.1331 & 0.0081 \\ \midrule
            \( \hat{\gamma}_5 \) & 0.0625 & 0.0704 & 0.0079 & 0.0600      & 0.0025 & 0.0587 & 0.0038 \\ \midrule
            \( \hat{\gamma}_6 \) & 0.0312 & 0.0350 & 0.0037 & 0.0252      & 0.0061 & 0.0268 & 0.0045 \\ \bottomrule
        \end{tabular}
    \end{subtable}
    \bigskip
    \begin{subtable}[t]{\textwidth}
        \centering
        \caption{Estimation of \( \hat{\gamma} \) parameters for \( N = 100000 \) samples.}%
        \label{tab:lab10-gamma-100000}
        \begin{tabular}{lccccccc}
            \toprule
            Parameter            & Real   & Linear & Error  & Hammerstein & Error  & Wiener & Error  \\ \midrule
            \( \hat{\gamma}_1 \) & 1.0000 & 1.0000 & 0.0000 & 1.0000      & 0.0000 & 1.0000 & 0.0000 \\ \midrule
            \( \hat{\gamma}_2 \) & 0.5000 & 0.5020 & 0.0020 & 0.4998      & 0.0002 & 0.4967 & 0.0033 \\ \midrule
            \( \hat{\gamma}_3 \) & 0.2500 & 0.2507 & 0.0007 & 0.2510      & 0.0010 & 0.2484 & 0.0016 \\ \midrule
            \( \hat{\gamma}_4 \) & 0.1250 & 0.1250 & 0.0000 & 0.1259      & 0.0009 & 0.1249 & 0.0001 \\ \midrule
            \( \hat{\gamma}_5 \) & 0.0625 & 0.0667 & 0.0042 & 0.0682      & 0.0057 & 0.0647 & 0.0022 \\ \midrule
            \( \hat{\gamma}_6 \) & 0.0312 & 0.0319 & 0.0006 & 0.0296      & 0.0016 & 0.0296 & 0.0016 \\ \bottomrule
        \end{tabular}
    \end{subtable}
\end{table}
As the number of samples increases, the error between the real and estimated
values decreases. For smaller numbers of samples, the estimated values deviate
from the real values; however, for larger numbers of samples, the estimated
values converge to the real values. The estimated values for the Hammerstein and
Wiener systems were scaled to ensure that the first element is equal to 1. The
scaling factor was calculated by dividing the first element of the real vector
by the first element of the estimated vector.

\end{document}
