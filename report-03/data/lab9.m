close all;
clear;

N = 1000000;

U_k = 2 * rand(N+3, 1) - 1;
W_k = zeros(N+3, 1);
V_k = zeros(N, 1);
Y_k = zeros(N, 1);
Z_k = rand(N, 1) - 0.5;

G_star = [3; 2; 1];
C = [3; 2];

Theta = [
    G_star(1) * C(1);
    G_star(1) * C(2);
    G_star(2) * C(1);
    G_star(2) * C(2);
    G_star(3) * C(1);
    G_star(3) * C(2);
];

for k = 1:N+3
    W_k(k) = C(1) * U_k(k) + C(2) * U_k(k)^2;
end

for k = 1:N
    V_k(k) = G_star(1) * W_k(k+2) + G_star(2) * W_k(k+1) + G_star(3) * W_k(k);
end

for k = 1:N
    Y_k(k) = V_k(k) + Z_k(k);
end

Phi_k = zeros(N, 6, 1);

for k = 1:N
    Phi_k(k,:) = [f1(U_k(k+2)), f2(U_k(k+2)), f1(U_k(k+1)), f2(U_k(k+1)), f1(U_k(k)), f2(U_k(k))];
end

Theta_hat = inv(transpose(Phi_k) * Phi_k) * transpose(Phi_k) * Y_k;

M = [
    G_star(1) * C(1), G_star(2) * C(1), G_star(3) * C(1);
    G_star(1) * C(2), G_star(2) * C(2), G_star(3) * C(2);
];

[P, D, Q] = svd(M);

disp("----------");
disp("Real:");
disp(Theta);
disp("Estimated:");
disp(Theta_hat);
disp("----------");
disp("----------");
disp("Real:");
disp(C);
disp("Estimated:");
disp(P(:,1));
disp("Scale:");
disp(C./P(:,1));
disp("----------");
disp("----------");
disp("Real:");
disp(G_star);
disp("Estimated:");
disp(Q(:,1));
disp("Scale:");
disp(G_star./Q(:,1));
disp("----------");

