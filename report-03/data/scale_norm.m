function [output] = scale_norm(X)
%SCALE_NORM Summary of this function goes here
%   Detailed explanation goes here

output = zeros(length(X), 1);
scale = X(1) / 1;

for i = 1:length(X)
    output(i) = X(i) / scale;
end
end