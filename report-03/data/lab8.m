close all;
clear;
clc;

N = 100000;
H = 1;

U_k = 2 * rand(N+3, 1) - 1;
Y_k = zeros(N, 1);

U = linspace(-6, 6, 100);
R = zeros(length(U), 1);
R_est = zeros(length(U), 1);

G_star = [3; 2; 1];

for k = 1:N
    x_k = G_star(1) * U_k(k+2) + G_star(2) * U_k(k+1) + G_star(3) * U_k(k);
    Y_k(k) = x_k^2 + x_k + rand() - 0.5;
end

g_k_first = zeros(3, 3);
g_k_second = zeros(3, 1);

for k = 1:N
    phi_k = [U_k(k+2); U_k(k+1); U_k(k)];
    g_k_first = g_k_first + (phi_k * transpose(phi_k) * kern(phi_k, H));
    g_k_second = g_k_second + (phi_k * Y_k(k) * kern(phi_k, H));
end

G_est = inv(g_k_first) * g_k_second;

disp(G_star);
disp(G_est);

X = zeros(N, 1);

for k = 1:N
    X(k) = G_est(1) * U_k(k+2) + G_est(2) * U_k(k+1) + G_est(3) * U_k(k);
end

for i = 1:length(U)
    up = 0;
    down = 0;
    for k = 1:N
        kernel_value = kernel(X(k), U(i), 0.7);
        up = up + (Y_k(k) * kernel_value);
        down = down + kernel_value;
    end
    R_est(i) = up / down;
end

for i = 1:length(U)
    R(i) = U(i)^2 + U(i);
end

figure;
hold on;
grid on;
plot(U, R, "DisplayName", "Real", "LineWidth", 1.5);
plot(U, R_est, "DisplayName", "Estimated", "LineWidth", 1, "Marker", ".");
legend("location", "best");
title("Estimation for N = " + N + " H = " + 0.7);
