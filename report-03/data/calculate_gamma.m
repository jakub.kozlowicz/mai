function [output] = calculate_gamma(n, tau_k, u_k, y_k)
%calculate_gamma Summary of this function goes here
%   Detailed explanation goes here

gamma = zeros(length(tau_k), 1);

for i = 1:length(tau_k)
    tau = tau_k(i);

    sum_tau = 0;
    for k = 1:n-tau
        sum_tau = sum_tau + (u_k(k) * y_k(k+tau));
    end

    gamma(i) = sum_tau / (n - tau);
end

output = gamma;
end