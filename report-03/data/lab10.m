close all;
clear;
clc;

N = 100000;

disp("Samples N = " + N);

tau_k = linspace(0, 5, 6);
U_k = randn(N, 1);
g_real = [1; 1/2; 1/4; 1/8; 1/16; 1/32];

%% Linear

Y_k = zeros(N, 1);

for k = 2:N
    Y_k(k) = 0.5 * Y_k(k-1) + U_k(k);
end

g_est = calculate_gamma(N, tau_k, U_k, Y_k);
g_est = scale_norm(g_est);
error = abs(g_real - g_est);

disp("--- Linear ---");
disp("Estimation:");
disp(g_est);
disp("Error:");
disp(error);

%% Hammerstein

W_k = zeros(N, 1);
Y_k = zeros(N, 1);

for k = 1:N
    W_k(k) = 3 * U_k(k) + 2 * U_k(k)^2;
end

for k = 2:N
    Y_k(k) = 0.5 * Y_k(k-1) + W_k(k);
end

g_est = calculate_gamma(N, tau_k, U_k, Y_k);
g_est = scale_norm(g_est);
error = abs(g_real - g_est);

disp("--- Hammerstein ---");
disp("Estimation:");
disp(g_est);
disp("Error:");
disp(error);

%% Weiner

X_k = zeros(N, 1);
Y_k = zeros(N, 1);

for i = 2:N
    X_k(i) = 0.5 * X_k(i-1) + U_k(i);
end
for i = 1:N
    Y_k(i) = 3 * X_k(i) + 2 * X_k(i)^2;
end

g_est = calculate_gamma(N, tau_k, U_k, Y_k);
g_est = scale_norm(g_est);
error = abs(g_real - g_est);

disp("--- Weiner ---");
disp("Estimation:");
disp(g_est);
disp("Error:");
disp(error);