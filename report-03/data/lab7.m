close all;
clear;

N = 100000;

B_k_1 = zeros(length(N), 1);
B_k_2 = zeros(length(N), 1);
B_k_3 = zeros(length(N), 1);
B_k_4 = zeros(length(N), 1);

P_0 = [
    100 0 0 0;
    0 100 0 0;
    0 0 100 0;
    0 0 0 100
    ];

B_star = [4; 3; 2; 1];
B_0 = [0; 0; 0; 0];

U_k = rand(N+4, 1);
Y_k = zeros(N+4, 1);

for k = 1:N
    Y_k(k) = B_star(1) * U_k(k+3) + B_star(2) * U_k(k+2) + B_star(3) * U_k(k+1) ...
        + B_star(4) * U_k(k) + rand() - 0.5;
end

for k = 1:N
    if k == 1
        p = P_0;
        b = B_0;
    else
        p = P_k;
        b = B_hat;
    end
    phi_k = [U_k(k+3); U_k(k+2); U_k(k+1); U_k(k)];
    P_k = p - ((p * phi_k * transpose(phi_k) * p)/(1 + transpose(phi_k) * p * phi_k));
    B_hat = b + P_k * phi_k * (Y_k(k) - transpose(phi_k) * b);
    B_k_1(k) = B_hat(1);
    B_k_2(k) = B_hat(2);
    B_k_3(k) = B_hat(3);
    B_k_4(k) = B_hat(4);
end

for k = 1:N
    B_k_1_real(k) = B_star(1);
    B_k_2_real(k) = B_star(2);
    B_k_3_real(k) = B_star(3);
    B_k_4_real(k) = B_star(4);
end

disp(B_star);
disp([B_k_1(end); B_k_2(end); B_k_3(end); B_k_4(end)]);

figure;
subplot(2,2,1);
hold on;
grid on;
plot(B_k_1, "DisplayName", "Estimated", "LineWidth", 1.5);
plot(B_k_1_real, "DisplayName", "Real", "LineWidth", 1.5, "LineStyle", "--");
xlabel("Number of k");
ylabel("Estimated value");
title("Estimation of B0");
legend("Location", "best");

subplot(2,2,2);
hold on;
grid on;
plot(B_k_2, "DisplayName", "Estimated", "LineWidth", 1.5);
plot(B_k_2_real, "DisplayName", "Real", "LineWidth", 1.5, "LineStyle", "--");
xlabel("Number of k");
ylabel("Estimated value");
title("Estimation of B1");
legend("Location", "best");

subplot(2,2,3);
hold on;
grid on;
plot(B_k_3, "DisplayName", "Estimated", "LineWidth", 1.5);
plot(B_k_3_real, "DisplayName", "Real", "LineWidth", 1.5, "LineStyle", "--");
xlabel("Number of k");
ylabel("Estimated value");
title("Estimation of B2");
legend("Location", "best");

subplot(2,2,4);
hold on;
grid on;
plot(B_k_4, "DisplayName", "Estimated", "LineWidth", 1.5);
plot(B_k_4_real, "DisplayName", "Real", "LineWidth", 1.5, "LineStyle", "--");
xlabel("Number of k");
ylabel("Estimated value");
title("Estimation of B3");
legend("Location", "best");
